/*
Use the reduce method in combination with the concat method to “flatten”
an array of arrays into a single array that has all the elements of the original
arrays.
*/

// let temp = [];

// function arraytoarr(arr) {
//     let result = [];
//     if (Array.isArray(arr)) {
//         arr.forEach(el => {
//             return temp.push(arraytoarr(el));
//         });
//     } else {
//         temp.push(arr);
//     }
//     return result;
// }

function flatten (arr) {
    let result = [];
    arr.forEach(element => {
        if (Array.isArray(element)) {
            element.forEach(el => {
                result.push(el);
            });
        } else {
            result.push(element);
        }
    });
    return result;
}

const array1 = [0,1,2,3,[4,5,6,7],[8,9,10]];

console.log(flatten(array1));