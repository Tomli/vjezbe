/*
Minimum
The previous chapter introduced the standard function Math.min that returns
its smallest argument. We can build something like that now. Write a function
min that takes two arguments and returns their minimum.
*/

const readlineSync = require('readline-sync');
 
const a = readlineSync.question('Enter a number: ');
const b = readlineSync.question('Enter another number: ');

if (isNaN(a) || isNaN(b)) {
    console.log('One of the given inputs is not a number.');
    return;
}

function GetMin(a,b) { // js doesn't like numbers confirmed
   if (Number(a) > Number(b)) {
        return b;
    } else {
        return a;
    }
}

console.log(`The smallest number from ${a} & ${b} is: ${GetMin(a,b)}`);