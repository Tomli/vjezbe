/*
We’ve seen that % (the remainder operator) can be used to test whether a
number is even or odd by using % 2 to see whether it’s divisible by two. Here’s
another way to define whether a positive whole number is even or odd:
55

• Zero is even.
• One is odd.
• For any other number N, its evenness is the same as N - 2.

Define a recursive function isEven corresponding to this description. The
function should accept a single parameter (a positive, whole number) and return
a Boolean.
Test it on 50 and 75. See how it behaves on -1. Why? Can you think of a
way to fix this?
*/

function isEven(number) {
    //number = Number(number);
    /*if (number < 0) { // Check if number is negative, if it is, make it a positive number
        number *= -1;
    }*/
    if (number === 0) {
        return true;
    } else if (number === 1) {
        return false;
    } else {
        return isEven(number-2);
    }
}

if(isEven(4)) {
    console.log("It's even.");
} else {
    console.log("It's not even.");
}