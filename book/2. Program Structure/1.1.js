/*
Write a loop that makes seven calls to console.log to output the following
triangle:
#
##
###
####
#####
######
#######
*/

for (let i = 1; i < 8; i++) {
    let msg = '';
    for (let b = 0; b < i; b++) {
        msg += '#';
    }
    console.log(msg);
}
