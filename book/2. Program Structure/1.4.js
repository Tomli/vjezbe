/*
 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #
 # # # #
# # # #

When you have a program that generates this pattern, define a binding size
= 8 and change the program so that it works for any size, outputting a grid
of the given width and height
*/

const readlineSync = require('readline-sync');
 
const input = readlineSync.question('Enter the width and height of the square: ');

if (isNaN(input)) {
    console.log('Given input is not a number.');
    return;
}

for (let i = 0; i < input; i++) {
    let msg = '';
    if(i % 2 == 0 || i == 0) {
        msg = ' ';
    }
    for (let b = 0; b < input; b++) {
        msg += '# ';
    }
    console.log(msg);
}