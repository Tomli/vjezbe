/*
Write a class Vec that represents a vector in two-dimensional space. It takes
x and y parameters (numbers), which it should save to properties of the same
name.
Give the Vec prototype two methods, plus and minus, that take another
vector as a parameter and return a new vector that has the sum or difference
of the two vectors’ (this and the parameter) x and y values.
Add a getter property length to the prototype that computes the length of
the vector—that is, the distance of the point (x, y) from the origin (0, 0).
*/

class vector2 {
    constructor(x, y) {
      this.x = x;
      this.y = y;
    }
  
    plus(other) {
      return new vector2(this.x + other.x, this.y + other.y);
    }
  
    minus(other) {
      return new vector2(this.x - other.x, this.y - other.y);
    }
  
    get length() {
      return Math.sqrt(this.x * this.x + this.y * this.y);
    }
  }

console.log(new vector2(1, 2).plus(new vector2(2, 3))); // triba vector2 { x: 3, y: 5}
console.log(new vector2(1, 2).minus(new vector2(2, 3))); // triba vector2 { x: -1, y: -1}
console.log(new vector2(3, 4).length); //triba 5