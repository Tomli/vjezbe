const roads = [
    "Alice's House-Bob's House", "Alice's House-Cabin",
    "Alice's House-Post Office", "Bob's House-Town Hall",
    "Daria's House-Ernie's House", "Daria's House-Town Hall",
    "Ernie's House-Grete's House", "Grete's House-Farm",
    "Grete's House-Shop", "Marketplace-Farm",
    "Marketplace-Post Office", "Marketplace-Shop",
    "Marketplace-Town Hall", "Shop-Town Hall"
];

function buildGraph(edges) {
    let graph = Object.create(null);
    function addEdge(from, to) {
        if (graph[from] == null) {
            graph[from] = [to];
        } else {
            graph[from].push(to);
        }
    }
    for (let [from, to] of edges.map(r => r.split("-"))) {
        addEdge(from, to);
        addEdge(to, from);
    }
    return graph;
}

const roadGraph = buildGraph(roads);

//console.log(roadGraph);

class VillageState {
    constructor(place, parcels) {
        this.place = place;
        this.parcels = parcels;
    }
    move(destination) {
        if (!roadGraph[this.place].includes(destination)) {
            return this;
        } else {
            let parcels = this.parcels.map(p => {
                if (p.place != this.place) return p;
                return {place: destination, address: p.address};
            }).filter(p => p.place != p.address);
            return new VillageState(destination, parcels);
        }
    }
}

// let first = new VillageState(
//     "Post Office",
//     [{place: "Post Office", address: "Alice's House"}]
// );

// let next = first.move("Alice's House");

// console.log(next.place); // → Alice's House
// console.log(next.parcels); // → []
// console.log(first.place); // → Post Office

function runRobot(state, robot, memory) {
    for (let turn = 0;; turn++) {
        if (state.parcels.length == 0) {
            console.log(`Done in ${turn} turns`);
            break;
        }
        let action = robot(state, memory);
        state = state.move(action.direction);
        memory = action.memory;
        console.log(`Moved to ${action.direction}`);
    }
}

function randomPick(array) {
    let choice = Math.floor(Math.random() * array.length);
    return array[choice];
}
function randomRobot(state) {
    return {direction: randomPick(roadGraph[state.place])};
}

VillageState.random = function(parcelCount = 5) {
    let parcels = [];
    for (let i = 0; i < parcelCount; i++) {
        let address = randomPick(Object.keys(roadGraph));
        let place;
        do {
            place = randomPick(Object.keys(roadGraph));
        } while (place == address);
        parcels.push({place, address});
    }
    return new VillageState("Post Office", parcels);
};

//runRobot(VillageState.random(), randomRobot);

const mailRoute = [
    "Alice's House", "Cabin", "Alice's House", "Bob's House",
    "Town Hall", "Daria's House", "Ernie's House",
    "Grete's House", "Shop", "Grete's House", "Farm",
    "Marketplace", "Post Office"
];

function routeRobot(state, memory) {
    if (memory.length == 0) {
        memory = mailRoute;
    }
    return {direction: memory[0], memory: memory.slice(1)};
}

function findRoute(graph, from, to) {
    let work = [{at: from, route: []}];
    for (let i = 0; i < work.length; i++) {
        let {at, route} = work[i];
        for (let place of graph[at]) {
            if (place == to) return route.concat(place);
            if (!work.some(w => w.at == place)) {
            work.push({at: place, route: route.concat(place)});
            }
        }
    }
}

function goalOrientedRobot({place, parcels}, route) {
    if (route.length == 0) {
        let parcel = parcels[0];
        if (parcel.place != place) {
            route = findRoute(roadGraph, place, parcel.place);
        } else {
            route = findRoute(roadGraph, place, parcel.address);
        }
    }
    return {direction: route[0], memory: route.slice(1)};
}

/*
------------------
      Task 1
------------------
It’s hard to objectively compare robots by just letting them solve a few scenarios. Maybe one robot just happened to get easier tasks or the kind of tasks
that it is good at, whereas the other didn’t.
Write a function compareRobots that takes two robots (and their starting memory). It should generate 100 tasks and let each of the robots solve each
of these tasks. When done, it should output the average number of steps each
robot took per task.
For the sake of fairness, make sure you give each task to both robots, rather
than generating different tasks per robot.
*/

function stepsCounter(robot, state, memory) {
    let steps = 0;
    while (true) {
        const temp = robot(state, memory);
        state = state.move(temp.direction);
        memory = temp.memory;
        steps++;
        if(state.parcels.length == 0) return steps;
    }
}

function compareRobots(robot1, memory1, robot2, memory2) {
    let robot1steps = 0;
    let robot2steps = 0;
    for (let i = 0; i < 100; i++) {
        let state = VillageState.random();
        robot1steps += stepsCounter(robot1, state, memory1);
        robot2steps += stepsCounter(robot2, state, memory2);
    }
    console.log('First Robot took ' + robot1steps + ' steps to complete 100 tasks');
    console.log('Goat Oriented Robot took ' + robot2steps + ' steps to complete 100 tasks');
}
  
//compareRobots(routeRobot, [], goalOrientedRobot, []);

/*
------------------
      Task 2
------------------
Can you write a robot that finishes the delivery task faster than goalOrientedRobot
? If you observe that robot’s behavior, what obviously stupid things does it
do? How could those be improved?
If you solved the previous exercise, you might want to use your compareRobots
function to verify whether you improved the robot.
*/

/* ===================== [ IDEA 1 ] ===================== */

// function checkAroundMyRobot(state) {
//     for (let name in roadGraph) {
//         if(state.place == name) {
//             //console.log('Found it! (Place: ' + state.place + ' | Name: ' + name + ')');
//             //console.log(roadGraph[name]);
//             for (let address in roadGraph[name]) {
//                 for (let i = 0; i < state.parcels.length; i++) {
//                     //console.log(state.parcels[i].address + ' | ' + roadGraph[name][address]);
//                     if(state.parcels[i].address == roadGraph[name][address]) {
//                         //console.log('He got a parcel to deliver right next to him!');
//                         return roadGraph[name][address];
//                     }
//                 }
//             }
//         }
//     }
//     return false;
// }

/* ===================== [ ROAD GRAPH ] =====================
{
    "Alice's House": [ "Bob's House", 'Cabin', 'Post Office' ],
    "Bob's House": [ "Alice's House", 'Town Hall' ],
    Cabin: [ "Alice's House" ],
    'Post Office': [ "Alice's House", 'Marketplace' ],
    'Town Hall': [ "Bob's House", "Daria's House", 'Marketplace', 'Shop' ],
    "Daria's House": [ "Ernie's House", 'Town Hall' ],
    "Ernie's House": [ "Daria's House", "Grete's House" ],
    "Grete's House": [ "Ernie's House", 'Farm', 'Shop' ],
    Farm: [ "Grete's House", 'Marketplace' ],
    Shop: [ "Grete's House", 'Marketplace', 'Town Hall' ],
    Marketplace: [ 'Farm', 'Post Office', 'Shop', 'Town Hall' ]
}
   ===================== [ ROAD GRAPH ] =====================*/

/*
function myRobot({place, parcels}, route) {
    if (route.length == 0) {
        let parcel = parcels[0];
        if (parcel.place != place) {
            if(!checkAroundMyRobot({place, parcels})) {
                console.log('Hey! A good destination would be: ' + checkAroundMyRobot({place, parcels}));
                route = findRoute(roadGraph, place, parcel.place);
            } else {
                route = findRoute(roadGraph, place, checkAroundMyRobot({place, parcels}));
            }
            console.log(route);
        } else {
            route = findRoute(roadGraph, place, parcel.address);
        }
    }
    return {direction: route[0], memory: route.slice(1)};
}
*/

// compareRobots(myRobot, [], goalOrientedRobot, []);

/* ===================== [ IDEA 1 ] ===================== */




/* ===================== [ IDEA 2 ] ===================== */
// const roads = [
//     "Alice's House-Bob's House", "Alice's House-Cabin",
//     "Alice's House-Post Office", "Bob's House-Town Hall",
//     "Daria's House-Ernie's House", "Daria's House-Town Hall",
//     "Ernie's House-Grete's House", "Grete's House-Farm",
//     "Grete's House-Shop", "Marketplace-Farm",
//     "Marketplace-Post Office", "Marketplace-Shop",
//     "Marketplace-Town Hall", "Shop-Town Hall"
// ];

function reverseArray(arr) {
    let result = [];
    const len = arr.length;
    for (let i = arr.length-1; i >= 0; i--) {
        result.push(arr[i]);
    }
    return result;
}

// const mailRoute = [
//     "Alice's House", "Cabin", "Alice's House", "Bob's House",
//     "Town Hall", "Daria's House", "Ernie's House",
//     "Grete's House", "Shop", "Grete's House", "Farm",
//     "Marketplace", "Post Office"
// ];

/*
function myRobot(state, memory) {
    if (memory.length == 0) {
        memory = mailRoute.concat(reverseArray(mailRoute));
    }
    return {direction: memory[0], memory: memory.slice(1)};
}

compareRobots(myRobot, [], goalOrientedRobot, []); 
*/

/* ===================== [ IDEA 2 ] ===================== */

/* ===================== [ IDEA 3 ] ===================== */

function getDistanceToLocation(location) {
    return findRoute(roadGraph, "Post Office", location).length;
}

function sortByDistance(arr) {
    let sortedarray = [];
    for (let index = 0; index < 10; index++) {
        for (const key in arr) {
            if(arr[key].distance == index) {
                sortedarray.push(arr[key]);
            }
        }
    }
    return sortedarray;
}

function calculateRobotRoute(parcels) {
    let parcelplaces = [];
    for (const key in parcels) {
        let temp = {};
        temp.address = parcels[key].address;
        temp.distance = getDistanceToLocation(parcels[key].address);
        parcelplaces.push(temp);
    }
    const sortedroute = sortByDistance(parcelplaces);
    let result = [];
    for (const key in sortedroute) {
        result.push(sortedroute[key].address);
    }
    return result;
}

let passedfirst = false;

function myRobot(state, memory) {
    if (memory.length == 0 && !passedfirst) {
        passedfirst = true;
        memory = mailRoute;
    } else if (memory.length == 0 && state.parcels.length != 0) {
        memory = calculateRobotRoute(state.parcels);
    }
    return {direction: memory[0], memory: memory.slice(1)};
}

compareRobots(myRobot, [], goalOrientedRobot, []); 

/* ===================== [ IDEA 3 ] ===================== */