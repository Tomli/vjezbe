/*
Write a CommonJS module, based on the example from Chapter 7, that contains the array of roads and exports the graph data structure representing them
as roadGraph. It should depend on a module ./graph, which exports a function
buildGraph that is used to build the graph. This function expects an array of
two-element arrays (the start and end points of the roads)
*/

module.exports.buildGraph = (edges) => {
    let graph = Object.create(null);
    function addEdge(from, to) {
        if (graph[from] == null) {
            graph[from] = [to];
        } else {
            graph[from].push(to);
        }
    }
    for (let [from, to] of edges.map(r => r.split("-"))) {
        addEdge(from, to);
        addEdge(to, from);
    }
    return graph;
}