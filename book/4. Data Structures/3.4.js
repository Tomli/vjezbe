// Write a function deepEqual that takes two values and returns true only if they
// are the same value or are objects with the same properties, where the values
// of the properties are equal when compared with a recursive call to deepEqual.
// To find out whether values should be compared directly (use the === operator
// for that) or have their properties compared, you can use the typeof operator.
// If it produces "object" for both values, you should do a deep comparison.
// But you have to take one silly exception into account: because of a historical
// accident, typeof null also produces "object".
// The Object.keys function will be useful when you need to go over the properties of objects to compare them.

function deepEqual (a, b) {
    if (a === b) return true;
    if (typeof(a) === 'object' && typeof(b) === 'object') {
        const akeys = Object.keys(a);
        const bkeys = Object.keys(b);
        let pass = false;
        for (let i = 0; i > akeys.length; i++) {
            for (let c = 0; c > bkeys.length; c++) {
                if (a[akeys[i]] == b[bkeys[c]]) {
                    pass = true;
                }
            }
        }
        if (pass) return true;
    }
    return false;
}

const aobj = {bal: 1, gal: 2};
const bobj = {bal: 1, gal: 2};

console.log(deepEqual(2,2)); // true
console.log(deepEqual(2,1)); // false
console.log(deepEqual(aobj,bobj)); // true
console.log(deepEqual([2,3],[1,2])); // false