// Arrays have a reverse method that changes the array by inverting the order in
// which its elements appear. For this exercise, write two functions, reverseArray
// and reverseArrayInPlace. The first, reverseArray, takes an array as argument
// and produces a new array that has the same elements in the inverse order. The
// second, reverseArrayInPlace, does what the reverse method does: it modifies
// the array given as argument

function reverseArray(arr) {
    let result = [];
    const len = arr.length;
    for (let i = arr[len-1]; i > 0; i--) {
        result.push(i);
    }
    return result;
}

function reverseArrayInPlace(arr) {
    const len = arr.length;
    for (let i = 0; i < len; i++) {
        const temp = arr[len-1];
        arr.splice(len-1,1);
        arr.splice(i,0,temp);
    }
}

let arr = [1,2,3,4,5,6,7];

console.log(reverseArray(arr));

reverseArrayInPlace(arr);

console.log(arr);