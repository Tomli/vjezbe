// Write a function arrayToList that builds up a list structure like the one
// shown when given [1, 2, 3] as argument. Also write a listToArray function
// that produces an array from a list. Then add a helper function prepend, which
// takes an element and a list and creates a new list that adds the element to the
// front of the input list, and nth, which takes a list and a number and returns
// the element at the given position in the list (with zero referring to the first
// element) or undefined when there is no such element.
// If you haven’t already, also write a recursive version of nth.

function arrayToList (arr) {
    let list = {value: arr[0], rest: null};
    arr.forEach(element => {
        const temp = list.rest;
        list.rest = {value: element, rest: temp};
    });
    return list;
}

function listToArray (list) {
    let rest = list;
    let arr = [];
    while (rest.rest != null) {
        rest = rest.rest;
        arr.push(rest.value);
    }
    return arr;
}

function prepend (list, value) {
    return {value: value, rest: list};
}

function nth (list, value, index = 0) {
    if (list.value === value) {
        return index;
    } else {
        return nth(list.rest, value, index+1);
    }
}

console.log(arrayToList([1,2,3]));

console.log(listToArray(arrayToList([1,2,3])));

const listt = arrayToList([1,2,3]);

console.log(nth(listt, 1));