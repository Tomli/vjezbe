const readlineSync = require('readline-sync');
 
const number = readlineSync.question('Enter a number: ');

if(isNaN(number)) {
    console.log('Input is not a number.');
    return;
}

let znamenke = [];

let num = number;

let sum = 0;

while (num > 0) {
    let p = Math.floor(num % 10);
    znamenke.push(p);
    num = Math.floor(num / 10);
}

var msg = `Number ${number} | `;

for (let i = znamenke.length-1; i >= 0; i--) {
    if(i != 0) msg += `${znamenke[i]} + `;
    else msg += `${znamenke[i]}`;
    sum += znamenke[i];
}

console.log(`${msg} = ${sum}`);