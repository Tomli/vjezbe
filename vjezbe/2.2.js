var readlineSync = require('readline-sync');
 
const number = readlineSync.question('Enter a number: ');

if(isNaN(number)) {
    console.log('Input is not a number.');
    return;
}

for (let i = number; i < 16*number; i++) {
    console.log(`${i}`);
}