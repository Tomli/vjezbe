const originalarr = [
    { address: 'Shop', distance: 2 },
    { address: "Ernie's House", distance: 4 },
    { address: "Bob's House", distance: 2 },
    { address: "Ernie's House", distance: 4 },
    { address: 'Post Office', distance: 2 }
];

function sortByDistance(arr) {
    let sortedarray = [];
    for (let index = 0; index < 10; index++) {
        for (const key in arr) {
            if(arr[key].distance == index) {
                sortedarray.push(arr[key]);
            }
        }
    }
    return sortedarray;
}

console.log(sortByDistance(originalarr));