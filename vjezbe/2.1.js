var readlineSync = require('readline-sync');
 
const number = readlineSync.question('Enter a number: ');

if(isNaN(number)) {
    console.log('Input is not a number.');
    return;
}

if(number % 2) {
    console.log('The number entered is not even.');
} else {
    console.log('The number entered is even.');
}