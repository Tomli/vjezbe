//Zadatak 10. Napisite program koji s tipkovnice ucitava dva cijela broja A i B, te ispisuje pravokutnik sastavljen od znaka *,
// sirine A i visine B

var readlineSync = require('readline-sync');
 
const width = readlineSync.question('Enter a width: ');
const length = readlineSync.question('Enter a length: ');

if(isNaN(width) || isNaN(length)) {
    console.log('Input is not a number.');
    return;
}

console.log(`Width: ${width} | Length: ${length}`);

for (let i = 0; i < length; i++) {
    var msg = '';
    for (let b = 0; b < width; b++) {
        msg = msg + '*';
    }
    console.log(`${msg}`);
}